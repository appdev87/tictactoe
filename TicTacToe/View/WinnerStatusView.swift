//
//  WinnerStatusView.swift
//  TicTacToe
//
//  Created by {[ ] [ ]} on 25.09.20.
//

import SwiftUI

/// View displaying the winner's score, icon, score, and TextField to enter their name whilst providing extra functionalities such as `Save Score`, `Replay Game` (without saving previous game data), and `Exit Game`.
struct WinnerStatusView: View {
     
    var winnerScore: Int
    var movesLeft: Int
    var winnerIcon: String
    var currentPlayer: String
    @State var name: String = ""
    
    /// Gets the button which is tapped on and sets the variable to desired View through the NavigationLink.
    @State var selection: String? = ""
    /// Instance variable used to determined if the **back** Button in the NavigationLink must display or depending if the View is displayed from the main menu or after `Save Score` Button is tapped.
    @State var hideBackBarBtn: Bool = false
    /// Instance variable of `ScoreListViewModel` used to display the score.
    @State var score = ScoreListViewModel()
    /// Instance variable used to save the score.
    @State var db: DBManager = DBManager()
    
    /// Receives the parsed-in data from `GameView` and creates a new table.
    init(player: String, winnerScore: Int, movesLeft: Int, winnerIcon: String) {
        self.currentPlayer = player
        self.winnerScore = winnerScore
        self.movesLeft = movesLeft
        self.winnerIcon = winnerIcon
        
        db.createTable()
    }
    
    var body: some View {
        
        NavigationView {
            Group {
                
                VStack {
                    
                    VStack{
                        ZStack {
                            
                            Image(self.winnerIcon)
                                .resizable()
                                .frame(width: 200, height: 200)
                            
                            VStack {
                                TitleText(title: "\(self.currentPlayer)",
                                          strokeThickness: 0.8,
                                          strokeColor: .white)
                                    .foregroundColor(.black)
                                    .font(Font.custom("GAMERIA", size: 40))
                                
                                TitleText(title: "Moves Left: \(self.movesLeft)",
                                          strokeThickness: 0.8,
                                          strokeColor: .white)
                                    .foregroundColor(.black)
                                    .font(Font.custom("GAMERIA", size: 30))
                                
                                TitleText(title: "Points: \(self.winnerScore)",
                                          strokeThickness: 0.8,
                                          strokeColor: .white)
                                    .foregroundColor(.black)
                                    .font(Font.custom("GAMERIA", size: 20))
                            }
                        }
                    }
                    
                    TextField("Enter name", text: self.$name)
                        .font(Font.custom("GAMERIA", size: 50))
                        .padding()
                    
                    // MARK: - Save Score button
                    NavigationLink(
                        destination: HighScoreView(hideBackBarBtn: self.hideBackBarBtn),
                        tag: "HighScoreView",
                        selection: self.$selection,
                        label: {
                            GameButton(title: "Save Score",
                                       viewName: "HighScoreView",
                                       iconName: "plateIcon",
                                       iconSize: 40,
                                       fontColor: .white,
                                       padding: 10).onTapGesture {
                                        
                                            let now = Date()
                                            let dateFormatter = DateFormatter()
                                            dateFormatter.dateStyle = .short
                                            dateFormatter.timeStyle = .short
                                            dateFormatter.locale = Locale(identifier: "en_GB")
                                            let currentDate = dateFormatter.string(from: now)
                                            
                                            self.score.saveScore(name: self.name,
                                                                 score: self.winnerScore,
                                                                 movesLeft: self.movesLeft,
                                                                 image: self.winnerIcon,
                                                                 date: currentDate)
                                            
                                            self.db.insert(name: self.name,
                                                           score: String(self.winnerScore),
                                                           movesLeft: String(self.movesLeft),
                                                           image: self.winnerIcon,
                                                           date: currentDate)
                                            
                                            self.selection = "HighScoreView"
                                            
                                            self.hideBackBarBtn = true
                                       }
                        })
                    
                    
                    // MARK: - Replay Game button
                    NavigationLink(destination: GameView(),
                                   tag: "GameView",
                                   selection: self.$selection,
                                   label: {
                                    GameButton(title: "Replay Game",
                                               viewName: "GameView",
                                               iconName: "replayIcon",
                                               iconSize: 40,
                                               fontColor: .white,
                                               padding: 10).onTapGesture {
                                                    self.selection = "GameView"
                                               }
                                   })
                    
                    // MARK: - Exit Game button
                    /**
                     Button to exit the game using the template of GameButton with different parameters for configuration.
                     SeeAlso: `GameButton.swift`
                     */
                    GameButton(title: "EXIT GAME",
                               viewName: "",
                               iconName: "backIcon",
                               iconSize: 40,
                               fontColor: .red,
                               padding: 20).onTapGesture(count: 1, perform: {
                                exit(0)
                               })
                }
            }
        }
    }
}
