//
//  HighScoreView.swift
//  TicTacToe
//
//  Created by {[ ] [ ]} on 24.09.20.
//

import SwiftUI

/// View to display all the `saved scores`. Can be accessed from the `HomeScreenView` or after saving a score through the `WinnerStatusView`.
struct HighScoreView: View {
    /// Instance of the `DBManager` class to retrieve the scores
    let db = DBManager()
    /// Variable used to determined if the **back** Button in the NavigationLink must display or depending if the View is displayed from the main menu or after `Save Score` Button is tapped.
    var hideBackBarBtn: Bool
    
    
    var body: some View {
        
        List {
            ScrollView {
                ForEach(self.db.getScores(), id: \.id) { playerScore in
                    HStack {
                        Image(playerScore.image)
                            .resizable()
                            .frame(width: 40, height: 40)
                        Spacer()
                        HStack {
                            VStack(alignment: .leading) {
                                CustomText(title: "\(playerScore.name)", size: 30, padding: .horizontal)
                                CustomText(title: "Score: \(playerScore.score)", size: 15, padding: .horizontal)
                            }
                        }
                        
                        CustomText(title: "\(playerScore.movesLeft)", size: 20, padding: .horizontal)
                        Spacer()
                        CustomText(title: "\(playerScore.date)", size: 20, padding: .trailing)
                            .frame(width: 100, height: 100, alignment: .trailing)
                    }
                }
            }
        }
        .ignoresSafeArea()
        .navigationBarTitle("SCORES", displayMode: .large)
        .navigationBarBackButtonHidden(self.hideBackBarBtn)
        
    }
}

struct HighScoreView_Previews: PreviewProvider {
    static var previews: some View {
        HighScoreView(hideBackBarBtn: false)
            .preferredColorScheme(.dark)
            .previewDevice("iPhone 11")
    }
}
