//
//  GameView.swift
//  TicTacToe
//
//  Created by {[ ] [ ]} on 14.08.20.
//

import SwiftUI

/// The main View for the Game
struct GameView: View {
    /// The amount of moves left. Is set to `9` on initialisation.
    @State var movesLeft: Int = 9
    /// Stores the score of `Player` one
    @State var score1: Int = 0
    /// Stores the score of `Player` two
    @State var score2: Int = 0
    /// Gets the current player. Is chosen at random on initialisation.
    /// Note: If the `CaseIterable` protocol is removed, `Player` cannot access the `allCases` function.
    @State var currentPlayer: Player = Player.allCases.randomElement()!
    /// The squareState is set to `o` by default.
    @State var squareState: SquareState = .o
    /// Instance of the `GameLogic` class.
    @State var gameLogic: GameLogic = GameLogic()
    /// Variable used to present types of alert if the selection made by the `Player` is empty and is chaged accordingly.
    @State var isSelectionEmpty: Bool = false
    /// Variable used to present types of alert if the game is tied and is changed accordingly.
    @State var isGameTied = false
    /// Used to alert if the is tied or the selection is not empty. See usage in `checkWin` and `nextMove` functions as well as applicability in the Button alert to use the `presentAlert()` function.
    @State var alertOccured = false
    /// Boolean variable to determine if a winner has been found or not.
    @State var haveAWinner: Bool = false
    /// Variable used to store the winner score and parsed into the `WinnerStatusView` init() before being saved.
    /// - SeeAlso: `WinnerStatusView.swift`
    @State var winner: Int = 0
    
    /*:
     Work around to fix a bug for the NavigationBar concerning the backBtn issues as the NavigationView only displays within the 'sheet' instead of dismissing the screen.
     For more information, see [Custom Back Button - StackOverflow.](https://stackoverflow.com/questions/56571349/custom-back-button-for-navigationviews-navigation-bar-in-swiftui/)
     */
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    private var backBtn: some View {
        Button(action: {
            self.presentationMode.wrappedValue.dismiss()
        }, label: {
            Image("homeIcon")
                .resizable()
                .frame(width: 20, height: 20)
            CustomText(title: "Home Screen", size: 30, padding: .horizontal)
                .foregroundColor(.secondary)
        })
    }
    
    var body: some View {
        NavigationLink(destination: HomeScreenView()) {
        }
        
        // MARK: - Top Title
        VStack {
            CustomText(title: "\(self.currentPlayer.rawValue) \(self.movesLeft == 9 ? " STARTS" : " 'S TURN")",
                       size: 25,
                       padding: .horizontal)
                .padding(5)
            
            TitleText(title: "MOVES LEFT: \(self.movesLeft)", strokeThickness: 0.8, strokeColor: .white)
                .foregroundColor(.black)
                .font(Font.custom("GAMERIA", size: 40))
            // Points by each player
            HStack(spacing: 20) {
                CustomText(title: "PLAYER 1: \(self.score1)", size: 25, padding: .horizontal)
                CustomText(title: "PLAYER 2: \(self.score2)", size: 25, padding: .horizontal)
            }
            
            Spacer()
            
            // MARK: - Game Board
            LazyVStack(spacing: 1) {
                ForEach(0..<3) { row in
                    HStack(spacing: 1) {
                        ForEach(0..<3) { col in
                            ZStack {
                                Button(action: {
                                    self.nextMove(for: self.currentPlayer, at: "\(row)\(col)")
                                }, label: {
                                    /// - Callers: `getCurrentSquareState(at position: String)`
                                    Image(self.getCurrentSquareState(at: "\(row)\(col)"))
                                        .resizable()
                                        .scaledToFit()
                                        .cornerRadius(20)
                                        .padding(3)
                                }).alert(isPresented: self.$alertOccured, content: {
                                    return self.presentAlert()
                                }).sheet(isPresented: self.$haveAWinner,
                                         onDismiss: { self.newGame() },
                                         content: {
                                            /// Parses all the details required to saved in the DB
                                            /// If it is the second player's turn means that Player 1 won
                                            WinnerStatusView(player: self.currentPlayer == .two ? "You win" : "You Lose",
                                                             winnerScore: self.winner,
                                                             movesLeft: self.movesLeft,
                                                             winnerIcon: self.squareState.rawValue)
                                         })
                                
                            }
                        }
                    }
                }
                .padding()
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(leading: self.backBtn)
                
            }
            Spacer()
            
            // MARK: Replay Game BUTTON
            GameButton(title: "Replay Game",
                       viewName: "",
                       iconName: "replayIcon",
                       iconSize: 40,
                       fontColor: .red,
                       padding: 0).onTapGesture(count: 1, perform: {
                        self.newGame()
                       })
        }
    }
    
    // MARK: newGame() function
    /**
     Resets all the values for the game to restart. It is called when the `GameView()` is loaded or when the `replayButton` is called in the NavigationLink.
     
     - Note: All previously stored score is reset to their default value but the **database** is not affected.
     
     - SeeAlso: `nextMove(for player: Player, at position: String)`
     */
    func newGame() {
        self.gameLogic.boardState = [:]
        self.currentPlayer = Player.allCases.randomElement()!
        self.score1 = 0
        self.score2 = 0
        self.movesLeft = 9
    }
    
    // MARK: - presentAlert() function
    /**
     Presents the alert according to the type of error that takes place.
     
     - Note: As per swiftUI 2.0, swift 5.3 and iOS 14.0, multiple alerts is not possible.
     
     - SeeAlso: Caller in the board.
     
     - Returns: An `Alert` according to the incorrect selection made or Game is tied.
     */
    func presentAlert() -> Alert {
        
        if self.isSelectionEmpty {
            return Alert(title: Text("☣️CURRENT SELECTION IS NOT EMPTY!!!⚠️"),
                         message: Text("Please selection an empty square"),
                         dismissButton: .cancel())
        }
        else {
            return Alert(title: Text("GAME TIED⁉️"),
                         message: Text("Want to find out the REAL WINNER or JUST QUIT😜"),
                         primaryButton: .default(Text("Start New Game")) {
                            self.newGame()
                         },
                         secondaryButton: .default(Text("EXIT GAME"), action: {
                            exit(0)
                         }))
        }
    }
    
    // MARK: getCurrentSquareState() function
    /**
     Takes in the position of the first and returns a String.
     
     - Parameter position: Is the selected position on the board.
     
     - Returns:  The raw-value of the SquareState to be parsed-in as a String for the Image.
     */
    func getCurrentSquareState(at position: String) -> String {
        self.gameLogic.getSquareState(for: position).rawValue
    }
    
    // MARK: - nextMove() function
    /**
     Determines the next move to be carried out as well as calls in the *checkWin()* function. The following function decreases the **movesLeft** counter.
     
     - Parameters:
         - player:   The current **Player** 1 (.one) or 2 (.two).
         - position: Determines the position that has been selected by the player.
     
     - SeeAlso: `checkWin(for square: SquareState, with moves: Int)`
     */
    private func nextMove(for player: Player, at position: String) {
        
        self.isSelectionEmpty = (self.gameLogic.getSquareState(for: position) == .empty)
        self.alertOccured = !self.isSelectionEmpty
        
        if self.isSelectionEmpty {
            // Sets the first move to `o`
            if self.movesLeft == 9 {
                self.squareState = .o
            }
            
            self.movesLeft -= 1
            
            if player == .one {
                self.score1 += 5
            }
            else {
                self.score2 += 5
            }
            
            self.gameLogic.setSquareState(self.squareState, at: position)
            self.checkWin(for: self.gameLogic.getSquareState(for: position), with: self.movesLeft)
            
            self.isSelectionEmpty = false
            
            // Toggles the square state and current `Player`
            self.currentPlayer.toggle()
            self.squareState.toggle()
        }
        else {
            self.isSelectionEmpty = true
        }
    }
    
    
    // MARK: - checkWin(player:, position:) function
    /**
     Checks if there is a winner using the selection made in the `nextMove()` function using the recently selected square.
     
     - Parameters:
         - square:   The current state of the selected square.
         - movesLeft: The amount of moves that can still be played until the game is tied if there is no winner beforehand.
     
     - SeeAlso: `nextMove(for player: Player, at position: String)`
     */
    func checkWin(for square: SquareState, with movesLeft: Int) {
        self.haveAWinner = self.gameLogic.checkWin(for: square)
        self.isGameTied = (movesLeft == 0)
        
        if self.haveAWinner {
            self.winner = (self.score1 > score2) ? self.score1 : self.score2
            // fix because it toggles before parsing the value to the WinnerStatusView
            self.squareState.toggle()
        }
        
        if !self.haveAWinner {
            self.alertOccured = self.isGameTied
        }
    }
}

/// Used only for development purposes but not necessary in production.
struct GameView_Previews: PreviewProvider {
    static var previews: some View {
        GameView()
            .preferredColorScheme(.dark)
            .previewDevice("iPhone 11")
    }
}
