//
//  HomeScreenView.swift
//  TicTacToe
//
//  Created by {[ ] [ ]} on 14.08.20.
//  Copyright © 2020 Hector Code. All rights reserved.
//

import SwiftUI

// MARK - Main View for TicTacToe App
struct HomeScreenView: View {
    
    /// Gets the button which is tapped on and sets the variable to desired View through the NavigationLink
    @State private var selection: String? = ""
    
    var body: some View {
        NavigationView {
            VStack {
                ZStack {
                    Image("samurai")
                        .resizable()
                        .frame(width: 250, height: 250, alignment: .topLeading)
                        .rotationEffect(Angle(degrees: 45))
                        .padding(-10)
                    
                    TitleText(title: "T I C T A C T O E",
                              strokeThickness: 0.8,
                              strokeColor: .white)
                        .foregroundColor(.black)
                        .font(Font.custom("GAMERIA", size: 50))
                }
                
                // MARK: - New Game button
                NavigationLink(
                    destination: GameView(),
                    tag: "GameView",
                    selection: $selection,
                    label: {
                        GameButton(title: "New Game",
                                   viewName: "GameView",
                                   iconName: "playIcon",
                                   iconSize: 40,
                                   fontColor: .white,
                                   padding: 20).onTapGesture(count: 1, perform: {
                                    selection = "GameView"
                                   })
                        
                    })
                
                // MARK: - High Score button
                NavigationLink(
                    destination: HighScoreView(hideBackBarBtn: false),
                    tag: "HighScoreView",
                    selection: $selection,
                    label: {
                        GameButton(title: "High Score",
                                   viewName: "HighScoreView",
                                   iconName: "highScoreIcon",
                                   iconSize: 40,
                                   fontColor: .secondary,
                                   padding: 20).onTapGesture(count: 1, perform: {
                                    selection = "HighScoreView"
                                   })
                        
                    })
                
                // MARK: - Exit Game button
                GameButton(title: "EXIT GAME",
                           viewName: "",
                           iconName: "backIcon",
                           iconSize: 40,
                           fontColor: .red,
                           padding: 20).onTapGesture(count: 1, perform: {
                            exit(0)
                           })
            }
        }.navigationBarBackButtonHidden(true)
    }
}

/// Used only for development purposes but not necessary in production.
struct HomeScreenView_Previews: PreviewProvider {
    static var previews: some View {
        HomeScreenView()
    }
}
