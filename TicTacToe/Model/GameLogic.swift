//
//  GameLogic.swift
//  TicTacToe
//
//  Created by {[ ] [ ]} on 15.09.20.
//

import Foundation
import SwiftUI
import os.log

/// Struct containing all the logic of the game including `Square` state, as well as getting and setting the state of the `Square`. It also contains the `checkWin` function which determines if the player won or not.
struct GameLogic {

    /// Dictionary of the board saving the position as `key` and state as `value`.
    var boardState: [String: SquareState] = [:]
    
    // MARK: - getSquareState()
    /**
     The function checks into dictionary of *boardState* to see of the position is already taken or not.
     
     - Parameter key: Takes the parsed-in position from the GameView to get the state of the current Button
     */
    func getSquareState(for key: String) -> SquareState {
        if let value = boardState[key] {
            return value
        }
        else {
            return .empty
        }
    }
    
    // MARK: - setSquareState()
    /**
     Sets the position and the state of the board only if the square is empty
     
     - Parameters:
         - state: Sets the between *x* or *o* and adds it to the boardState
         - position: Get the position as a String of the clicked position
     
     - SeeAlso: `nextMove(for player: Player, at position: String)`
     */
    mutating func setSquareState(_ state: SquareState, at position: String) {
        if boardState[position] != nil {
            os_log("Position \(position) IS ALREADY TAKEN!!!")
        } else {
            self.boardState[position] = state
        }
    }
    
    // MARK: - checkWin() function
    /**
     Checks if the last move made by the player is a win or not. It uses a two-dimesional array of `String` containing all the winning combinations possible. The `winningCombinations` is used as a template and conpared to the parsed value which is an array containing the player's moves. The parsed-in `Array<String>` is converted to a `Set` to gain access to the `intersection` function for comparing the two sets. As both `Array` and `Set` uses the same protocol, only the parsed-in array needs to be converted. To determine the winning combination, the capacity of the `newSet` is used to determined if a winner has been found or not. The `log` is only used for code coverage in the console. The `map` function found in the `log` uses `closure` functionalities to facilitate the output.
     
     - Parameter player: The square that was selected by the player.
     
     - Returns: A boolean is see if there's a win or not.
     
     - SeeAlso: [`intersection()`](https://developer.apple.com/documentation/swiftui/accessibilitytraits/intersection(_:))
     */
    func checkWin(for player: SquareState) -> Bool {
        
        let filteredArr: [String: SquareState] = boardState.filter { $0.value == player }
        
        let filteredArray: [String] = filteredArr.keys.sorted()
        os_log("<=************ CHECK WIN ********************=>")
        os_log("Player: \(player.rawValue) -> Parsed array:  \(filteredArray)")
        
        let winningCombination: Array<Array<String>> = [["00", "11", "22"],
                                                        ["00", "01", "02"],
                                                        ["00", "10", "20"],
                                                        ["10", "11", "12"],
                                                        ["01", "11", "21"],
                                                        ["02", "12", "22"],
                                                        ["02", "11", "20"],
                                                        ["20", "21", "22"]]
        
        
        let set2: Set<String> = Set(filteredArray)
        
        for i in 0..<winningCombination.count {
            let newSet = set2.intersection(winningCombination[i])
            if newSet.count == 3 {
                os_log("Winning Combination: \(newSet.map { $0 })")
                return true
            }
        }
        return false
    }
    
}
