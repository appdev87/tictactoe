//
//  GameButton.swift
//  TicTacToed
//
//  Created by {[ ] [ ]} on 01.09.20.
//  Copyright © 2020 HectorCode. All rights reserved.
//

import Foundation
import SwiftUI

/**
 GameButton Struct used for button creation with multiple values using an OOP-approach for multiple button creation.
 - Parameters:
     - title: The title to be displayed on the button.
     - viewName: View to go to when the Button is clicked using the NavigationLink.
     - iconName: The name of the icon to use to display.
         - Important:
             The parsed name as the iconName must match the **exact** name of the icon in the Assets folder.
     - iconSize: The size of the icon.
     - fontColor: The color of the text to be displayed.
     - padding: The padding around the `Button`
 */
struct GameButton: View {

    let title: String
    let viewName: String
    let iconName: String
    let iconSize: CGFloat
    let fontColor: Color
    let padding: CGFloat
    
    var body: some View {
        RoundedRectangle(cornerRadius: 15)
            .frame(height: 80)
            .padding(30)
            .foregroundColor(.orange)
            .overlay(
                HStack {
                    Image(iconName)
                        .resizable()
                        .frame(width: iconSize, height: iconSize, alignment: .leading)
                    
                    Text(title)
                        .font(Font.custom("GAMERIA", size: 30))
                        .foregroundColor(fontColor)
                }
            )
            .padding(padding)
    }
}
