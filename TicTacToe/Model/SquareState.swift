//
//  SquareState.swift
//  TicTacToe
//
//  Created by {[ ] [ ]} on 01.09.20.
//  Copyright © 2020 HectorCode. All rights reserved.
//

import Foundation

/// `enum` conforming to the `String` (to give default values) protocol for the `SquareState`. Use to return feedback on the sate of the selected Button.
enum SquareState: String {
    case empty = "emptyIcon"
    case x = "swordIcon"
    case o = "circleIcon"
    
    // MARK: toggle()
    /**
     Function which switches between different states of `x` and `o`.
     
     - Note: Must be a mutating function to conform to `enum`.
     */
    mutating func toggle() {
        self = (self == .o) ? .x : .o
    }
}
