//
//  Player.swift
//  TicTacToe
//
//  Created by {[ ] [ ]} on 01.09.20.
//

import Foundation

/// `enum` conforming to the `String` (to give default values) and `CaseIterable` (to allow toggling) protocols for the `Player`s
enum Player: String, CaseIterable {
    /// one: Player one with default value
    case one = "Player 1"
    /// two: Player two with default value
    case two = "Player 2"
    
    // MARK: toggle()
    /**
     Function which switches between player one and two.
     
     - Note: Must be a mutating function to conform to `enum`.
     */
    mutating func toggle() {
        self = (self == .one) ? .two : .one
    }
}


