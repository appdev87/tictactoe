//
// Created by {[ ] [ ]} on 25.09.20.
//

import Foundation
import SwiftUI

/// Creates a custom ButtonStyle with specific frame size
struct CustomButtonStyle: ButtonStyle {
    /// MARK: - makeBody()
    /**
     Configures the appearance of the Button and returns a View.
     
     - Parameters configuration: default configuration parameters inheriting from the Configuration protocol
     - SeeAlso: [`makeBody(configuration:)`](https://developer.apple.com/documentation/swiftui/defaultbuttonstyle/makebody(configuration:))
     */
	func makeBody(configuration: Configuration) -> some View {
		configuration.label
				.frame(width: 80, height: 80)
	}
    
}
