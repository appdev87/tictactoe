//
//  CustomText.swift
//  TicTacToe
//
//  Created by {[ ] [ ]} on 24.09.20.
//

import SwiftUI

/// View used for custom text with having to call the associated functions `padding` and `font`.
struct CustomText: View {

    /// - title: The title used for the Text.
    let title: String
    /// size: The size of the font.
    let size: CGFloat
    /// padding: The padding from the current Text View to the next.
    let padding: Edge.Set

    var body: some View {
        Text(title)
            .padding(padding)
            .font(Font.custom("GAMERIA", size: size))
    }
}
