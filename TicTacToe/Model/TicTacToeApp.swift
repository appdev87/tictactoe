//
//  TicTacToeApp.swift
//  TicTacToe
//
//  Created by {[ ] [ ]} on 24.09.20.
//

import SwiftUI

/// As per iOS 14.0, the following struct replaces the `AppDelegate.swift` file for application launch.
@main
struct TicTacToeApp: App {
    var body: some Scene {
        WindowGroup {
            HomeScreenView()
                .preferredColorScheme(.dark)
                .previewDevice("iPhone 11")
            
        }
    }
}
