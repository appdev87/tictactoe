//
//  TitleText.swift
//  TicTacToed
//
//  Created by {[ ] [ ]} on 09.09.20.
//  Copyright © 2020 HectorCode. All rights reserved.
//

import SwiftUI


/**
 As per Swift 5.3, the Text contour is not available. This struct is used mainly for creating a contour around the main title. For more information, see [Make text stroke in SwiftUI](https://stackoverflow.com/questions/57334125/how-to-make-text-stroke-in-swiftui)
 */
struct TitleText: View {

    /// The title to be displayed.
    let title: String
    /// The thickness of the stroke.
    let strokeThickness: CGFloat
    /// The color of the foreground color.
    let strokeColor: Color

    var body: some View {
        ZStack{
            ZStack {
                Text(title).offset(x: strokeThickness, y: strokeThickness)
                Text(title).offset(x: -strokeThickness, y: -strokeThickness)
                Text(title).offset(x: -strokeThickness, y: strokeThickness)
                Text(title).offset(x: strokeThickness, y: -strokeThickness)
            }.foregroundColor(strokeColor)

            Text(title)
        }
    }
}
