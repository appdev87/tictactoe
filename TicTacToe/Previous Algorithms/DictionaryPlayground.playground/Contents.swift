import UIKit
import Foundation
import SwiftUI

enum ButtonState: String {
    case empty = ""
    case x = "swordIcon"
    case o = "circleIcon"
}


class GameLogic {
    
    var buttonState = ButtonState.self
    
    var gameState: [String: ButtonState] = [:]
    
    func getState(for key: String) -> ButtonState {
        print(gameState)
        if let value = gameState[key] {
            return value
        } else {
            return .empty
        }
    }
    
    func setState(_ state: ButtonState, for key: String) {
        gameState[key] = state
    }
    
    func checkWin() {
        
        if (self.gameState["00"] == self.gameState["11"]) && (self.gameState["00"] == self.gameState["22"]) {
            print("\(self.gameState["00"]!.self) WINS")
        }
        
        else if (self.gameState["00"] == self.gameState["01"]) && (self.gameState["00"] == self.gameState["02"]) {
            print("\(self.gameState["00"]!.self) WINS")
        }
        
        else if (self.gameState["00"] == self.gameState["10"]) && (self.gameState["00"] == self.gameState["20"]) {
            print("\(self.gameState["00"]!.self) WINS")
        }
    }
}


var logic1 = GameLogic()
logic1.setState(.x, for: "00")
logic1.setState(.x, for: "00")
logic1.setState(.o, for: "01")
logic1.setState(.x, for: "11")
logic1.setState(.o, for: "21")
logic1.setState(.x, for: "22")
logic1.setState(.o, for: "02")
logic1.setState(.x, for: "10")

logic1.checkWin()

