#  Folder containing previous algorithms

This  folder contains all the `algorithms` and `function` tests that have been previous created during the realisation of the project. 

They haven't been used because they contain some bugs or are not fully functional or do not conform to the `structs` when implementing with `Views` in SwiftUI. **Good for reference.**

