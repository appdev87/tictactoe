import UIKit
enum ButtonState: String {
    case empty = "emptyIcon"
    case x = "swordIcon"
    case o = "circleIcon"
}


class GameLogic {
    
    var buttonState = ButtonState.self
    
    var gameState: [String: ButtonState] = [:]
    
    func getState(for key: String) -> ButtonState {
        print(gameState)
        if let value = gameState[key] {
            return value
        }
        else {
            return .empty
        }
    }
    
    func setState(_ state: ButtonState, for key: String) {
        if gameState[key] != nil {
            print("Already occupied square")
        } else {
            gameState[key] = state
        }
    }

    
    /**
     
     */
    func checkWin(for player: ButtonState, moves: Int) -> Bool {
        let filteredArrX = gameState.filter { $0.value == player }

        let filteredArray: [String] = filteredArrX.keys.sorted()
        print("===========================================================")
        print(player, filteredArray)
        
        let winningCombination: [[String]] = [["00", "11", "22"], ["00", "01", "02"], ["00", "10", "20"], ["01", "11", "21"], ["02", "12", "22"], ["02", "11", "20"],["20", "21", "22"]]
//        let parsedCombination: [String] = ["11", "22", "01", "02", "00"].sorted()
        print(filteredArray)
        var newArr: [String] = [String]()
        
        if filteredArray.count < 3 {
            return false
        } else {
            
            for i in 0...filteredArray.count {
                for j in 0...winningCombination.count {
                    if winningCombination[j].contains(filteredArray[i]) {
                        
                        newArr.append(filteredArray[i])
                            if newArr.count >= 3 {
                                print(newArr)
                                print("\(player) WINS".uppercased())
                                return true
                            }
                        }
                        
                    }
                }
        }
        
//
//        for i in 0...winningCombination.count {
//            for j in 0...filteredArray.count {
//                if (winningCombination[i] == filteredArray[j]) {
//                    newArr.append(winningCombination[i])
//                    print(newArr)
//                }
//            }
//        }
            
//
//            if filteredArray.elementsEqual(["00", "11", "22"]) {
//                print("x wins")
//                return true
//            }
//            if filteredArray.elementsEqual(["00", "01", "02"]) {
//                print("x wins")
//                return true
//            }
//
//            if filteredArray.elementsEqual(["00", "10", "20"]) {
//                print("x wins")
//                return true
//            }
//
//            if filteredArray.elementsEqual(["01", "11", "21"]) {
//                print("x wins")
//                return true
//            }
//            if filteredArray.elementsEqual(["02", "12", "22"]) {
//                print("x wins")
//                return true
//            }
//
//            if filteredArray.elementsEqual(["02", "11", "20"]) {
//                print("x wins")
//                return true
//            }
//
//            if filteredArray.elementsEqual(["10", "11", "12"]) {
//                print("x wins")
//                return true
//            }
//
//            if filteredArray.elementsEqual(["20", "21", "22"]) {
//                print("x wins")
//                return true
//            }
        return false;
    }
}


var logic: GameLogic = GameLogic()
    logic.setState(.o, for: "00")
    logic.getState(for: "00")
    logic.checkWin(for: .o, moves: 0)

    logic.setState(.x, for: "00")
    logic.checkWin(for: .x, moves: 0)



    logic.setState(.x, for: "21")
    logic.checkWin(for: .x, moves: 0)

    logic.setState(.o, for: "22")
    logic.checkWin(for: .o, moves: 0)

    logic.setState(.o, for: "11")
    logic.checkWin(for: .o, moves: 0)

    logic.setState(.x, for: "12")
    logic.checkWin(for: .x, moves: 0)

