//
//  DBManager.swift
//  TicTacToe
//
//  Created by {[ ] [ ]} on 10.09.20.
//  Tutorial for sqlite3 in swift followed on [SQLite3 with Swift](https://www.raywenderlich.com/6620276-sqlite-with-swift-tutorial-getting-started)
//

import Foundation
import SQLite3
import os.log

/// Class for managing the SQLite3 database
class DBManager {
    
    /// Pointer for the C-API to store db connection pointer.
    var db: OpaquePointer?
    /// Variable storing the path to the `sqlite` database.
    var path: String = "myScore.sqlite"
    
    init() {
        // Using an adapted Proxy Design Pattern
        self.db = (self.db == nil) ? self.openConnection() : self.db
    }
    
    // MARK: openConnection()
    /**
     Opens the connection for sqlite3 using the default swift `SQLITE3` framework of the Core Data framework.
     
     - Returns: An OpaquePointer for the database in use.
     */
    func openConnection() -> OpaquePointer? {
        let filePath = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathExtension(self.path)
        print(filePath)
        var db: OpaquePointer? = nil
        
        if sqlite3_open(filePath.path, &db) != SQLITE_OK {
            os_log("🤖 An error occured in creation a connection.", type: .error)
            return nil
        }
        
        else {
            os_log("📟 Database ACCESS GRANTED!!!", type: .info)
            return db
        }
        
    }
    
    // MARK: createTable()
    /**
     Creates the table using the `query` statement in the function with default attributes as per requirement. A `TABLE SUCCESSFULLY CREATED` log is displayed in the console and logged, otherwise an error log is displayed.
     */
    func createTable() {
        let query = """
                    CREATE TABLE IF NOT EXISTS score(
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    name VARCHAR(20),
                    score VARCHAR(10),
                    movesLeft VARHCHAR(10),
                    image VARCHAR(10),
                    date TEXT);
                    """
        
        var createTableStatement: OpaquePointer? = nil
        
        if sqlite3_prepare_v2(self.db, query, -1, &createTableStatement, nil) ==  SQLITE_OK {
            if sqlite3_step(createTableStatement) == SQLITE_DONE {
                os_log("✅ TABLE SUCCESSFULLY CREATED!!!", type: .info)
            }
            else {
                os_log("❌ ERROR CREATING TABLE!!!", type: .error)
            }
        }
        else {
            os_log("❌ TABLE CREATION FAILED!!!", type: .error)
        }
    }
    
    // MARK: - insert() function
    /**
     Inserts the parsed values into the Database. If everything went well, a `SUCCESS INSERTION` message is created.
     
     - Parameters:
         - name: The name of the player entered through the TextField.
         - score: The score of the winning player received from `GameView`.
         - movesLeft: The amount of moves left when the player won the game.
         - image: The image icon, i.e. `swordIcon` or `circleIcon`, of the winning player
         - date: The date and time at which the player won the game.
     
     - SeeAlso: `WinnerStatusView.swift` &amp; `Save Score Button `
     */
    func insert(name: String, score: String, movesLeft: String, image: String, date: String) {
        
        let query = """
                        INSERT INTO score(id, name, score, movesLeft, image, date)
                        VALUES (?, ?, ?, ?, ?, ?);
                    """
        var insertStatement: OpaquePointer? = nil
        
        var isEmptyTable = false
        
        if getScores().isEmpty {
            isEmptyTable = true
        }
        
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        let date = dateFormatter.string(from: now)
        
        if sqlite3_prepare_v2(db, query, -1, &insertStatement, nil) == SQLITE_OK {
            if isEmptyTable {
                sqlite3_bind_int(insertStatement, 1, 1)
            }
            sqlite3_bind_text(insertStatement, 2, (name as NSString).utf8String, -1, nil)
            sqlite3_bind_text(insertStatement, 3, (score as NSString).utf8String, -1, nil)
            sqlite3_bind_text(insertStatement, 4, (movesLeft as NSString).utf8String, -1, nil)
            sqlite3_bind_text(insertStatement, 5, (image as NSString).utf8String, -1, nil)
            sqlite3_bind_text(insertStatement, 6, (date as NSString).utf8String, -1, nil)
            
            if sqlite3_step(insertStatement) == SQLITE_DONE {
                os_log("✅ SUCCESS INSERTING DATA", type: .info)
            }
            else {
                os_log("❌ DATA NOT INSERTED!!!", type: .fault)
            }
        }
        else {
            os_log("❌ Query failed to execute!!!", type: .error)
        }
        
    }
    
    // MARK: - getScores()
    /**
     Retrieves all the scores that has been stored in the database. Logs a `SUCCESS` or `FAILURE` message. It complies to the `ScoreRowViewModel` struct to retrieve the data. 
     
     - Returns: An array of `ScoreRowViewModel` containing all the data stored in the database.
     */
    func getScores() -> [ScoreRowViewModel] {
        var playerScores = [ScoreRowViewModel]()
        let query = "SELECT * FROM score ORDER BY movesLeft ASC;"
        var getScoresStatement: OpaquePointer? = nil
        
        if sqlite3_prepare_v2(self.db, query, -1, &getScoresStatement, nil) == SQLITE_OK {
            while sqlite3_step(getScoresStatement) == SQLITE_ROW {
                let id = Int(sqlite3_column_int(getScoresStatement, 0))
                let name = String(describing: String(cString: sqlite3_column_text(getScoresStatement, 1)))
                let score = String(describing: String(cString: sqlite3_column_text(getScoresStatement, 2)))
                let movesLeft = String(describing: String(cString: sqlite3_column_text(getScoresStatement, 3)))
                let image = String(describing: String(cString: sqlite3_column_text(getScoresStatement, 4)))
                let date = String(describing: String(cString: sqlite3_column_text(getScoresStatement, 5)))
                
                let playerScore = ScoreRowViewModel()
                playerScore.id = id
                playerScore.name = name
                playerScore.score = Int(score)!
                playerScore.movesLeft = Int(movesLeft)!
                playerScore.image = image
                playerScore.date = date
                
                playerScores.append(playerScore)
            }
            os_log("✅ SUCCESS RETRIEVING DATA", type: .info)
        }
        else {
            os_log("❌ Error RETRIEVING SCORES!!!", type: .error)
        }
        
        return playerScores
        
    }
}
