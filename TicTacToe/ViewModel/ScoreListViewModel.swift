//
//  ScoreListController.swift
//  TicTacToe
//
//  Created by {[ ] [ ]} on 25.09.20.
//

import Foundation
import os.log

class ScoreListViewModel {
    
    /// List storing object of the `ScoreRowViewModel` type.
    var scoreList: [ScoreRowViewModel] = [ScoreRowViewModel]()
    
    /// MARK: saveScore()
    /**
     Saves the score in `scoreList` instance.
     - Parameters:
         - name: The name of the winning `Player`.
         - score: The score of the winning `Player`.
         - movesLeft:  The amount of moves left at moment of win.
         - image: The icon of the winner.
         - date: The date and time of the winner.
     */
    func saveScore(name: String, score: Int, movesLeft: Int, image: String, date: String) {
        let scoreRow = ScoreRowViewModel()
        scoreRow.name = name
        scoreRow.score = score
        scoreRow.movesLeft = movesLeft
        scoreRow.image = image
        scoreRow.date = date
        
        scoreList.append(scoreRow)
        os_log("Data saved: %@", type: .info)
        
    }
    
    /// MARK: retrieveAllScores()
    /**
     Retieves all the records from the `scoreList` as an array of `ScoreRowViewModel`.
     
     Returns: All the records from the `scoreList` as an array of `ScoreRowViewModel`
     */
    func retrieveAllScores() -> [ScoreRowViewModel] {
        return self.scoreList
    }
}

/// Provides the default structure for the information to be saved in the Database.
class ScoreRowViewModel {
    
    /// The row id in the table
    var id = 0
    /// The name of the winning `Player`
    var name = ""
    /// The score of the winning `Player`
    var score = 0
    /// The amount of moves left at moment of win
    var movesLeft = 0
    /// The icon of the winner
    var image = ""
    /// The date and time of the winner
    var date = ""
    
}
